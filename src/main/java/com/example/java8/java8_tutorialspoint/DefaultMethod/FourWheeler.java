/**
 * 10.5 afternoon
 * https://www.tutorialspoint.com/java8/java8_default_methods.htm
 */
package com.example.java8.java8_tutorialspoint.DefaultMethod;

public interface FourWheeler {
    default void print() {
        System.out.println("I am a four wheeler!");
    }
}
