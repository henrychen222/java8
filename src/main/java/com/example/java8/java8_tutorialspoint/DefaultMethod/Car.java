/**
 * 10.5 afternoon
 * https://www.tutorialspoint.com/java8/java8_default_methods.htm
 */
package com.example.java8.java8_tutorialspoint.DefaultMethod;

public class Car implements Vehicle, FourWheeler {
    public void print() {
        //call the default method of the specified interface using super
        Vehicle.super.print();
        FourWheeler.super.print();

        Vehicle.blowHorn();
        System.out.println("I am a car!");
    }
}
