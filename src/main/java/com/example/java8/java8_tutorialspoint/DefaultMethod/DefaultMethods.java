/**
 * 9.29 afternoon
 * https://www.tutorialspoint.com/java8/java8_default_methods.htm
 * <p>
 * a new concept of default method implementation in interfaces
 * old interfaces can be used to leverage the lambda expression capability of Java 8
 */
package com.example.java8.java8_tutorialspoint.DefaultMethod;

public class DefaultMethods {
    public static void main(String args[]) {
        Vehicle vehicle = new Car();
        vehicle.print();
    }
}

//interface Vehicle {
//    default void print() {
//        System.out.println("I am a vehicle!");
//    }
//
//    static void blowHorn() {
//        System.out.println("Blowing horn!!!");
//    }
//}

//interface FourWheeler {
//    default void print() {
//        System.out.println("I am a four wheeler!");
//    }
//}

//class Car implements Vehicle, FourWheeler {
//    public void print() {
//        //call the default method of the specified interface using super
//        Vehicle.super.print();
//        FourWheeler.super.print();
//
//        Vehicle.blowHorn();
//        System.out.println("I am a car!");
//    }
//}
