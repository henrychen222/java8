/**
 * 9.29 afternoon
 * https://www.tutorialspoint.com/java8/java8_functional_interfaces.htm
 * Functional interfaces have a single functionality to exhibit.
 *
 * functional interfaces defined in java.util.Function package
 * https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html
 * Each functional interface has a single abstract method, called the functional method
 */
package com.example.java8.java8_tutorialspoint;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class FunctionalInterfaces {
    public static void main(String args[]) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        System.out.println("Print all numbers:");
        eval(list, n -> true);

        System.out.println("Print even numbers:");
        eval(list, n -> n % 2 == 0);

        System.out.println("Print numbers greater than 3:");
        eval(list, n -> n > 3);
    }

    /**
     * Predicate <T> interface is a functional interface with a method test(Object) to return a Boolean value.
     * This interface signifies that an object is tested to be true or false
     */
    public static void eval(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer n : list) {
            if (predicate.test(n)) {
                System.out.println(n + " ");
            }
        }
    }
}
