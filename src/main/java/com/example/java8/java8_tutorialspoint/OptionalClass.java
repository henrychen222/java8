/**
 * 9.29 afternoon
 * https://www.tutorialspoint.com/java8/java8_optional_class.htm
 */
package com.example.java8.java8_tutorialspoint;

import java.util.Optional;

public class OptionalClass {
    public static void main(String args[]) {
        OptionalClass java8Tester = new OptionalClass();
        Integer value1 = null;
        Integer value2 = new Integer(10);

        /** ofNullable(): allows passed parameter to be null **/
        Optional<Integer> a = Optional.ofNullable(value1);

        /** of(): throws NullPointerException if passed parameter is null **/
        Optional<Integer> b = Optional.of(value2);
        System.out.println(java8Tester.sum(a, b));
    }

    public Integer sum(Optional<Integer> a, Optional<Integer> b) {
        /** isPresent(): checks the value is present or not **/
        System.out.println("First parameter is present: " + a.isPresent());
        System.out.println("Second parameter is present: " + b.isPresent());

        /** orElse(): returns the value if present otherwise returns the default value passed **/
        Integer value1 = a.orElse(new Integer(0));

        /** get(): gets the value, value should be present **/
        Integer value2 = b.get();
        return value1 + value2;
    }
}
