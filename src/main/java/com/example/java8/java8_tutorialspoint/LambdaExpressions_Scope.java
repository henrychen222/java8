/**
 * 9.28 evening
 * https://www.tutorialspoint.com/java8/java8_lambda_expressions.htm
 *
 * Using lambda expression, you can refer to any final variable or effectively final variable (which is assigned only once).
 * Lambda expression throws a compilation error, if a variable is assigned a value the second time.
 */
package com.example.java8.java8_tutorialspoint;

public class LambdaExpressions_Scope {

    final static String salutation = "Hello! ";

    interface GreetingService {
        void sayMessage(String message);
    }

    public static void main(String args[]) {
        GreetingService greetService1 = message ->
                System.out.println(salutation + message);
        greetService1.sayMessage("Mahesh");
    }

}
