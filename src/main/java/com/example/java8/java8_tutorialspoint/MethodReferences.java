/**
 * 9.28 evening
 * https://www.tutorialspoint.com/java8/java8_method_references.htm
 *
 * Method references help to point to methods by their names.
 * A method reference is described using "::" symbol.
 * A method reference can be used to point the following types of methods −
 *    Static method,
 *    Instance methods,
 *    Constructors using new operator (TreeSet::new)
 */
package com.example.java8.java8_tutorialspoint;

import java.util.ArrayList;
import java.util.List;

public class MethodReferences {

    public static void main(String args[]) {
        List names = new ArrayList();

        names.add("Mahesh");
        names.add("Suresh");
        names.add("Ramesh");
        names.add("Naresh");
        names.add("Kalpesh");

        // passed System.out::println method as a static method reference
        names.forEach(System.out::println);
    }
}
