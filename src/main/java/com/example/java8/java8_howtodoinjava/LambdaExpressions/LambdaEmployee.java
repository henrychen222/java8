/**
 * 10.2 evening
 * https://howtodoinjava.com/java8/lambda-expressions/
 */
package com.example.java8.java8_howtodoinjava.LambdaExpressions;

import java.util.Arrays;

public class LambdaEmployee {

    public static void main(String args[]) {
        /** 3 Sorting employees objects by their name **/
        Employee[] employees = {
                new Employee("David"),
                new Employee("Naveen"),
                new Employee("Alex"),
                new Employee("Richard")};
        System.out.println("Before Sorting Names: " + Arrays.toString(employees));
        Arrays.sort(employees, Employee::nameCompare);
        System.out.println("After Sorting Names " + Arrays.toString(employees));
    }
}
