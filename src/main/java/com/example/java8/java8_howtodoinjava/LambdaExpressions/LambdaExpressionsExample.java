/**
 * 10.2 evening
 * https://howtodoinjava.com/java8/lambda-expressions/
 */
package com.example.java8.java8_howtodoinjava.LambdaExpressions;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class LambdaExpressionsExample {

    public static void main(String args[]) {
        /** 1 Iterating over a List and perform some operations **/
        List<String> pointList = new ArrayList();
        pointList.add("1");
        pointList.add("2");

        pointList.forEach(p -> {
            System.out.println(p);
        });

        /** 2 Create a new runnable and pass it to thread **/
        new Thread(() -> System.out.println("My Runnable")).start();

        /** 4 Adding an event listener to a GUI component **/
        JButton button = new JButton("Submit");
        button.addActionListener((e) -> {
            System.out.println("Click event triggered !!");
        });

    }
}
