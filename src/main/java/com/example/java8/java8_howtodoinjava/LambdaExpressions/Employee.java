/**
 * 10.2 evening
 * https://howtodoinjava.com/java8/lambda-expressions/
 */
package com.example.java8.java8_howtodoinjava.LambdaExpressions;

public class Employee {
    String name;

    public Employee(String name) {
        this.name = name;
    }

    public static int nameCompare(Employee a1, Employee a2) {
        return a1.name.compareTo(a2.name);
    }

    public String toString() {
        return name;
    }

}
