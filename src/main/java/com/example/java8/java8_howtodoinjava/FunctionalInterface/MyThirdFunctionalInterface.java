/**
 * 10.2 evening
 * https://howtodoinjava.com/java8/functional-interface-tutorial/
 */
package com.example.java8.java8_howtodoinjava.FunctionalInterface;

/** If an interface declares an abstract method overriding one of the public methods of java.lang.Object,
 * that also does not count toward the interface’s abstract method count
 *
 * Comparator is a functional interface even though it declared two abstract methods,
 * Because one of these abstract methods “equals()” which has signature equal to public method in Object class
 */
@FunctionalInterface
public interface MyThirdFunctionalInterface {
    public void firstWork();

    @Override
    public String toString();                //Overridden from Object class

    @Override
    public boolean equals(Object obj);        //Overridden from Object class

}
