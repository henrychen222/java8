/**
 * 10.2 evening
 * https://howtodoinjava.com/java8/functional-interface-tutorial/
 */
package com.example.java8.java8_howtodoinjava.FunctionalInterface;

@FunctionalInterface
public interface MyFirstFunctionalInterface {

    public void firstWork();
    // public void doSomeMoreWork();   //error, functional interface can have only one one abstract method
}
