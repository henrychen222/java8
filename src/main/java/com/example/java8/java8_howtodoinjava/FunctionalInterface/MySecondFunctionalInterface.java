/**
 * 10.2 evening
 * https://howtodoinjava.com/java8/functional-interface-tutorial/
 */
package com.example.java8.java8_howtodoinjava.FunctionalInterface;

@FunctionalInterface
public interface MySecondFunctionalInterface {
    public void firstWork();

    /**
     * a functional interface has exactly one abstract method
     * default methods have an implementation, they are not abstract
     */
    default void doSomeMoreWork1() {
    }

    default void doSomeMoreWork2() {
    }
}
