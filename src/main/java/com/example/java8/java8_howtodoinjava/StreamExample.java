/**
 * 9.30 evening
 * https://howtodoinjava.com/java8/java-streams-by-examples/
 */
package com.example.java8.java8_howtodoinjava;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExample {

    public static void main(String[] args) {
        /** 2.2. Stream.of(val1, val2, val3….) **/
        System.out.println("-----2.2. Stream.of(val1, val2, val3….)-----");
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        stream.forEach(p -> System.out.println(p));

        /** 2.2. Stream.of(arrayOfElements) **/
        System.out.println("\n-----2.2. Stream.of(arrayOfElements)-----");
        Stream<Integer> stream2 = Stream.of(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
        stream2.forEach(p -> System.out.println(p));

        /** 2.3. List.stream() **/
        System.out.println("\n-----2.3. List.stream()-----");
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i < 10; i++) {
            list.add(i);
        }
        Stream<Integer> stream3 = list.stream();
        stream3.forEach(p -> System.out.println(p));

        /** 2.5 String chars or String tokens **/
        System.out.println("\n-----2.5 String chars or String tokens-----");
        IntStream stream5_chars = "12345_abcdefg".chars();
        stream5_chars.forEach(p -> System.out.println(p));
        Stream<String> stream5_tokens = Stream.of("A$B$C".split("\\$"));
        stream5_tokens.forEach(p -> System.out.println(p));

        /** 3.1. Convert Stream to List – Stream.collect( Collectors.toList() ) **/
        System.out.println("\n-----3.1. Convert Stream to List – Stream.collect( Collectors.toList() )-----");
        List<Integer> list_31 = new ArrayList<Integer>();
        for (int i = 1; i < 10; i++) {
            list_31.add(i);
        }
        Stream<Integer> stream_31 = list_31.stream();
        List<Integer> evenNumbersList = stream_31.filter(i -> i % 2 == 0).collect(Collectors.toList());
        System.out.print(evenNumbersList);

        /** 3.2. Convert Stream to array – Stream.toArray( EntryType[]::new ) **/
        System.out.println("\n\n-----3.2. Convert Stream to array – Stream.toArray( EntryType[]::new )-----");
        List<Integer> list_32 = new ArrayList<Integer>();
        for (int i = 1; i < 10; i++) {
            list_32.add(i);
        }
        Stream<Integer> stream_32 = list_32.stream();
        Integer[] evenNumbersArr = stream_32.filter(i -> i % 2 == 0).toArray(Integer[]::new);
        System.out.print(Arrays.toString(evenNumbersArr)); //print an array

        /** 4. Core stream operations **/
        List<String> memberNames = new ArrayList<>();
        memberNames.add("Amitabh");
        memberNames.add("Shekhar");
        memberNames.add("Aman");
        memberNames.add("Rahul");
        memberNames.add("Shahrukh");
        memberNames.add("Salman");
        memberNames.add("Yana");
        memberNames.add("Lokesh");

        // 4.1 Intermediate operations
        /** 4.1.1. Stream.filter() **/
        System.out.println("\n\n-----4.1.1. Stream.filter()-----");
        memberNames.stream().filter((s) -> s.startsWith("A"))
                .forEach(System.out::println);

        /** 4.1.1. Stream.map() **/
        System.out.println("\n-----4.1.1. Stream.map()-----");
        memberNames.stream().filter((s) -> s.startsWith("A"))
                .map(String::toUpperCase)
                .forEach(System.out::println);

        /** 4.1.1. Stream.sorted() **/
        System.out.println("\n-----4.1.1. Stream.sorted()-----");
        memberNames.stream().sorted().map(String::toUpperCase).forEach(System.out::println);

        // 4.2 Terminal operations
        /** 4.2.1. Stream.forEach() **/
        System.out.println("\n-----4.2.1. Stream.forEach()-----");
        memberNames.forEach(System.out::println);

        /** 4.2.2. Stream.collect() **/
        System.out.println("\n-----4.2.2. Stream.collect()-----");
        List<String> memNamesInUppercase = memberNames.stream().sorted()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.print(memNamesInUppercase);

        /** 4.2.3. Stream.match() **/
        System.out.println("\n\n-----4.2.3. Stream.match()-----");
        boolean matchedResult = memberNames.stream().anyMatch((s) -> s.startsWith("A"));
        System.out.println(matchedResult);

        matchedResult = memberNames.stream().allMatch((s) -> s.startsWith("A"));
        System.out.println(matchedResult);

        matchedResult = memberNames.stream().noneMatch((s) -> s.startsWith("A"));
        System.out.println(matchedResult);

        /** 4.2.4. Stream.count() **/
        System.out.println("\n-----4.2.4. Stream.count()-----");
        long totalMatched = memberNames.stream().filter((s) -> s.startsWith("A")).count();
        System.out.println(totalMatched);

        /** 4.2.5. Stream.reduce() **/
        System.out.println("\n-----4.2.5. Stream.reduce()-----");
        Optional<String> reduced = memberNames.stream().reduce((s1, s2) -> s1 + "#" + s2);
        reduced.ifPresent(System.out::println);

        //5. Stream short-circuit operations
        /** 5.1. Stream.anyMatch() **/
        System.out.println("\n-----5.1. Stream.anyMatch()-----");
        boolean matched = memberNames.stream().anyMatch((s) -> s.startsWith("A"));
        System.out.println(matched);

        /** 5.2. Stream.findFirst() **/
        System.out.println("\n-----5.2. Stream.findFirst()-----");
        String firstMatchedName = memberNames.stream().filter((s) -> s.startsWith("L")).findFirst().get();
        System.out.println(firstMatchedName);

        //6 Parallelism in Java Steam
        System.out.println("\n-----6 Parallelism in Java Steam-----");
        List<Integer> list6 = new ArrayList<Integer>();
        for (int i = 1; i < 10; i++) {
            list6.add(i);
        }
        Stream<Integer> parallelstream = list6.parallelStream(); // creating a parallel stream
        Integer[] evenNumbersArr6 = parallelstream.filter(i -> i % 2 == 0).toArray(Integer[]::new);
        System.out.print(Arrays.toString(evenNumbersArr6)); //print an array
    }
}
