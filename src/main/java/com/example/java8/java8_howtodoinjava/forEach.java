/**
 * 9.30 evening
 * https://howtodoinjava.com/java8/java-streams-by-examples/
 */
package com.example.java8.java8_howtodoinjava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class forEach {

    public static void main(String args[]) {
        /** 2. Java 8 stream forEach example **/
        ArrayList<Integer> numberList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        Consumer<Integer> action = System.out::println;
        numberList.stream().filter(n -> n % 2 == 0).forEach(action);

        System.out.println("");
        /** 3. Java forEach example using List **/
        ArrayList<Integer> numberList3 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        Consumer<Integer> action3 = System.out::println;
        numberList.forEach(action);

        System.out.println("");
        /** 4. Java forEach example using Map **/
        HashMap<String, Integer> map = new HashMap<>();
        map.put("A", 1);
        map.put("B", 2);
        map.put("C", 3);
        Consumer<Map.Entry<String, Integer>> action4 = System.out::println;
        map.entrySet().forEach(action4);

        Consumer<String> actionOnKeys = System.out::println;
        map.keySet().forEach(actionOnKeys);

        Consumer<Integer> actionOnValues = System.out::println;
        map.values().forEach(actionOnValues);

        System.out.println("");
        /** 5. Create custom action **/
        HashMap<String, Integer> map5 = new HashMap<>();
        map.put("A", 1);
        map.put("B", 2);
        map.put("C", 3);

        Consumer<Map.Entry<String, Integer>> action5 = entry ->
        {
            System.out.println("Key is : " + entry.getKey());
            System.out.println("Value is : " + entry.getValue());
        };

        map.entrySet().forEach(action5);
    }


}
