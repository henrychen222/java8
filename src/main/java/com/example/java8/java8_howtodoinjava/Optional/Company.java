/**
 * 10.5 afternoon
 * https://howtodoinjava.com/java8/java-8-optionals-complete-reference/
 */
package com.example.java8.java8_howtodoinjava.Optional;

public class Company {

    private Integer companyId;
    private String companyName;

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


}
