/**
 * 10.5 afternoon
 * https://howtodoinjava.com/java8/java-8-optionals-complete-reference/
 */
package com.example.java8.java8_howtodoinjava.Optional;

import java.util.Optional;

public class OptionalExample {

    public static void main(String args[]) {
        Optional<Integer> canBeEmpty1 = Optional.of(5);
        System.out.println(canBeEmpty1.isPresent());            // returns true
        System.out.println(canBeEmpty1.get());                  // returns 5

        Optional<Integer> canBeEmpty2 = Optional.empty();
        System.out.println(canBeEmpty2.isPresent());            // returns false

        /**
         * for print Optional object, don't use System.out.println() directly,
         * it will print Optional.empty, Optional[5].
         *
         * Instead,
         * if null, use isPresent(),
         * if exist, use get()
         */
        System.out.println(canBeEmpty1);
        System.out.println(canBeEmpty2);


        // a) Creating Optional objects
        System.out.println("\n------Creating Optional objects-------");
        /** Use Optional.empty() to create empty optional */
        Optional<Integer> possible_a1 = Optional.empty();
        System.out.println(possible_a1.isPresent());


        /** Use Optional.of() to create optional with default non-null value */
        Optional<Integer> possible_a2 = Optional.of(5);
        System.out.println(possible_a2.get());

        /** Use Optional.ofNullable() to create an Optional object that may hold a null value */
        Optional<Integer> possible_a3 = Optional.ofNullable(null);
        Optional<Integer> possible_a4 = Optional.ofNullable(5);
        System.out.println(possible_a3.isPresent());
        System.out.println(possible_a4.get());

        //b) Do something If Optional value is present
        System.out.println("\n------Do something If Optional value is present-------");
        Optional<Integer> possible_b = Optional.of(5);
        possible_b.ifPresent(System.out::println); //recommend
        // another way for print, not recommend
        if (possible_b.isPresent()) {
            System.out.println(possible_b.get());
        }

        //c) Default/absent values and actions
        System.out.println("\n------Default/absent values and actions-------");
        /** Assume this value has returned from a method */
        Optional<Company> companyOptional = Optional.empty();
        System.out.println(companyOptional.isPresent());

        /** Now check optional; if value is present then return it, else create a new Company object and return it */
        Company company = companyOptional.orElse(new Company());
        /** OR you can throw an exception as well */
        //Company company = companyOptional.orElseThrow(IllegalStateException::new);
        System.out.println(company.getCompanyId());
        System.out.println(company.getCompanyName());

        //d) Rejecting certain values using the filter method
        Optional<Company> companyOptional2 = Optional.empty();
//        companyOptional2.filter(department -> "Finance".equals(department.getCompanyName())
//                .ifPresent(() -> System.out.println("Finance is present")));


    }
}
