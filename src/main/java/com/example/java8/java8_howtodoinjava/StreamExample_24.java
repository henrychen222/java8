/**
 * 9.30 evening
 * https://howtodoinjava.com/java8/java-streams-by-examples/
 */
package com.example.java8.java8_howtodoinjava;

import java.util.Date;
import java.util.stream.Stream;

public class StreamExample_24 {

    public static void main(String[] args) {

        /** 2.4. Stream.generate() or Stream.iterate() **/
        System.out.println("\n");
        Stream<Date> stream4 = Stream.generate(() -> {
            return new Date();
        });
        stream4.forEach(p -> System.out.println(p));
    }
}

