/**
 * 10.7 night
 * https://howtodoinjava.com/java8/java-8-list-all-files-example/
 */
package com.example.java8.java8_howtodoinjava;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class iterateDirectory {

    public static void main(String args[]) throws IOException {

        /** 1. List all files and sub-directories using Files.list() */
        Files.list(Paths.get(".")).forEach(System.out::println);

        /** 2. List only files inside directory using filter expression */
        System.out.println("\n");
        Files.list(Paths.get(".")).filter(Files::isRegularFile).forEach(System.out::println);

        /** 3. List files and sub-directories with Files.newDirectoryStream() */
        System.out.println("\n");
        Files.newDirectoryStream(Paths.get("."))
                .forEach(System.out::println);

        /** 4. List only files with Files.newDirectoryStream() */
        System.out.println("\n");
        Files.newDirectoryStream(Paths.get("."), path -> path.toFile().isFile())
                .forEach(System.out::println);

        /** 5. List files of certain extention with Files.newDirectoryStream() */
        System.out.println("\n");
        Files.newDirectoryStream(Paths.get("."), path -> path.toString().endsWith(".xml"))
                .forEach(System.out::println);

        /** 6. Find all hidden files in directory */
        final File[] files = new File(".").listFiles(file -> file.isHidden());
        final File[] files_same = new File(".").listFiles(File::isHidden);
        System.out.println(Arrays.toString(files));
        System.out.println(Arrays.toString(files_same));

    }
}
