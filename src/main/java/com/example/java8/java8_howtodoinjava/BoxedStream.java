/**
 * 10.2 evening
 * https://howtodoinjava.com/java8/java8-boxed-intstream/
 */
package com.example.java8.java8_howtodoinjava;

import java.util.List;
import java.util.Optional;
import java.util.stream.*;

public class BoxedStream {

    public static void main(String args[]) {
        // convert stream of objects to collection
        List<String> strings = Stream.of("how", "to", "do", "in", "java").collect(Collectors.toList());
        System.out.println(strings);

        /**1. IntStream – stream of ints **/
        //Get the collection and later convert to stream to process elements
        List<Integer> ints = IntStream.of(1, 2, 3, 4, 5).boxed().collect(Collectors.toList());
        System.out.println(ints);

        //Stream operations directly
        Optional<Integer> max = IntStream.of(1, 2, 3, 4, 5).boxed().max(Integer::compareTo);
        System.out.println(max);

        /** 2. LongStream – stream of longs **/
        List<Long> longs = LongStream.of(1l, 2l, 3l, 4l, 5l).boxed().collect(Collectors.toList());
        System.out.println(longs);

        /** 3. DoubleStream – stream of doubles **/
        List<Double> doubles = DoubleStream.of(1d, 2d, 3d, 4d, 5d).boxed().collect(Collectors.toList());
        System.out.println(doubles);

    }
}
