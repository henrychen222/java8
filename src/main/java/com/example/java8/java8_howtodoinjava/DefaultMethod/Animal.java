/**
 * 10.4 afternoon
 * https://howtodoinjava.com/java8/default-methods-in-java-8/
 */
package com.example.java8.java8_howtodoinjava.DefaultMethod;

public class Animal implements Moveable{
    //override the default method
    public void move(){
        System.out.println("I am running");
    }

    public static void main(String[] args){
        Animal tiger = new Animal();
        tiger.move();
    }

}
