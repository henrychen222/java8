/**
 * 10.4 afternoon
 * https://howtodoinjava.com/java8/default-methods-in-java-8/
 */
package com.example.java8.java8_howtodoinjava.DefaultMethod;

import java.util.ArrayList;
import java.util.List;

public class Animal2 implements Moveable {
    public static void main(String[] args) {
        List<Animal2> list = new ArrayList();
        list.add(new Animal2());
        list.add(new Animal2());
        list.add(new Animal2());

        //Iterator code reduced to one line
        list.forEach((Moveable p) -> p.move());
    }
}
