/**
 * 10.4 afternoon
 * https://howtodoinjava.com/java8/default-methods-in-java-8/
 */
package com.example.java8.java8_howtodoinjava.DefaultMethod;

public interface Moveable {
    /**
     * Interface can only have abstract method, java8 allows default and static method
     */
    default void move() {
        System.out.println("I am moving");
    }

    static void stop(){
        System.out.println("I am stoping");
    }
}
