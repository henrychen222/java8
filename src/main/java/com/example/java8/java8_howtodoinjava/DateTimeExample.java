/**
 * 10.7 night
 * https://howtodoinjava.com/java8/date-and-time-api-changes-in-java-8-lambda/
 */
package com.example.java8.java8_howtodoinjava;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAdjusters;
import java.time.zone.ZoneRules;

public class DateTimeExample {

    public static void main(String args[]) {

        /** New classes to represent local date and timezone */
        /** LocalDate */
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate.toString());
        System.out.println(localDate.getDayOfWeek().toString());
        System.out.println(localDate.getDayOfMonth());
        System.out.println(localDate.getDayOfYear());
        System.out.println(localDate.isLeapYear());
        System.out.println(localDate.plusDays(12).toString());

        /** LocalTime */
        System.out.println("\n");
        LocalTime localTime = LocalTime.of(12, 20);
        System.out.println(localTime.toString());    //12:20
        System.out.println(localTime.getHour());     //12
        System.out.println(localTime.getMinute());   //20
        System.out.println(localTime.getSecond());   //0
        System.out.println(localTime.MIDNIGHT);      //00:00
        System.out.println(localTime.NOON);          //12:00

        System.out.println("");
        LocalTime localTime_now = LocalTime.now();
        System.out.println(localTime_now.toString());
        System.out.println(localTime_now.getHour());
        System.out.println(localTime_now.getMinute());
        System.out.println(localTime_now.getSecond());
        System.out.println(localTime_now.MIDNIGHT);
        System.out.println(localTime_now.NOON);

        /** LocalDateTime */
        System.out.println("\n");
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime.toString());
        System.out.println(localDateTime.getDayOfMonth());
        System.out.println(localDateTime.getHour());
        System.out.println(localDateTime.getNano());

        /** OffsetDateTime, ZonedDateTime
         * Timezone offset can be represented in “+05:30” or “Europe/Paris” formats
         * */
        System.out.println("\n");
        OffsetDateTime offsetDateTime = OffsetDateTime.now();
        System.out.println(offsetDateTime.toString());

        offsetDateTime = OffsetDateTime.now(ZoneId.of("+05:30"));
        System.out.println(offsetDateTime.toString());

        offsetDateTime = OffsetDateTime.now(ZoneId.of("-06:30"));
        System.out.println(offsetDateTime.toString());

        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        System.out.println(zonedDateTime.toString());


        /** New classes to represent timestamp and duration */
        /** Instant */
        System.out.println("\n");
        Instant instant = Instant.now();
        System.out.println(instant.toString());
        System.out.println(instant.plus(Duration.ofMillis(5000)).toString());
        System.out.println(instant.minus(Duration.ofMillis(5000)).toString());
        System.out.println(instant.minusSeconds(10).toString());

        /** Duration */
        System.out.println("\n");
        Duration duration = Duration.ofMillis(5000);
        System.out.println(duration.toString());     //PT5S
        duration = Duration.ofSeconds(60);
        System.out.println(duration.toString());     //PT1M
        duration = Duration.ofMinutes(10);
        System.out.println(duration.toString());     //PT10M
        duration = Duration.ofHours(2);
        System.out.println(duration.toString());     //PT2H
        duration = Duration.between(Instant.now(), Instant.now().plus(Duration.ofMinutes(10)));
        System.out.println(duration.toString());  //PT10M

        /** Period */
        System.out.println("\n");
        Period period = Period.ofDays(6);
        System.out.println(period.toString());    //P6D
        period = Period.ofMonths(6);
        System.out.println(period.toString());    //P6M
        period = Period.between(LocalDate.now(),
                LocalDate.now().plusDays(60));
        System.out.println(period.toString());   //P1M29D


        /** Added utility classes over existing enums */
        /** DayOfWeek: from 1 (Monday) to 7 (Sunday) */
        System.out.println("\n");
        System.out.println(DayOfWeek.of(2));     //TUESDAY
        DayOfWeek day = DayOfWeek.FRIDAY;
        System.out.println(day.getValue());      //5
        LocalDate ld = LocalDate.now();
        System.out.println(ld.with(DayOfWeek.MONDAY));


        /** Date adjusters */
        System.out.println("\n");
//        LocalDate date = LocalDate.of(2013, Month.MAY, 15);
        LocalDate date = LocalDate.now();
        LocalDate endOfMonth = date.with(TemporalAdjusters.lastDayOfMonth());
        System.out.println(endOfMonth.toString());
        LocalDate nextTue = date.with(TemporalAdjusters.next(DayOfWeek.TUESDAY));
        System.out.println(nextTue.toString());

        /** Creating date objects */
        System.out.println("\n");
        //Builder pattern used to make date object
        OffsetDateTime date1 = Year.of(2013).atMonth(Month.MAY).atDay(15).atTime(0, 0).atOffset(ZoneOffset.of("+03:00"));
        System.out.println(date1);
        //factory method used to make date object
        OffsetDateTime date2 = OffsetDateTime.of(2013, 5, 15, 0, 0, 0, 0, ZoneOffset.of("+03:00"));
        System.out.println(date2);

        /** New class to simulate system/machine clock */
        System.out.println("\n");
        Clock clock = Clock.systemDefaultZone();
        System.out.println(clock);
        System.out.println(clock.instant().toString());
        System.out.println(clock.getZone());

        Clock anotherClock = Clock.system(ZoneId.of("Europe/Tiraspol"));
        System.out.println(anotherClock);
        System.out.println(anotherClock.instant().toString());
        System.out.println(anotherClock.getZone());

        Clock forwardedClock = Clock.tick(anotherClock, Duration.ofSeconds(600));
        System.out.println(forwardedClock.instant().toString());


        /** Timezone Changes */
        System.out.println("\n");
        //Zone rules
        System.out.println(ZoneRules.of(ZoneOffset.of("+02:00")).isDaylightSavings(Instant.now()));
        System.out.println(ZoneRules.of(ZoneOffset.of("+02:00")).isFixedOffset());

        /** Date Formatting */
        System.out.println("\n");
        DateTimeFormatterBuilder formatterBuilder = new DateTimeFormatterBuilder();
        formatterBuilder.append(DateTimeFormatter.ISO_LOCAL_DATE_TIME).appendLiteral("-").appendZoneOrOffsetId();
        DateTimeFormatter formatter = formatterBuilder.toFormatter();
        System.out.println(formatter.format(ZonedDateTime.now()));


    }


}
